import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
		Player hasil = null;
		for(Player chara : player){
			if(chara.getName().equals(name)){
				hasil = chara;
			}
		}		
        return hasil;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
		String hasil = "";
		if(find(chara) != null){
			hasil = "Sudah ada karakter bernama " + chara;
		} else{
			if(tipe.equals("Human")){
				Human pemainBaru = new Human(chara, hp);
				player.add(pemainBaru);
				hasil = chara + " ditambah ke game";
			} else if(tipe.equals("Magician")){
				Magician pemainBaru = new Magician(chara, hp);
				player.add(pemainBaru);
				hasil = chara + " ditambah ke game";
			} else{
				Monster pemainBaru = new Monster(chara, hp, "AAAAAAaaaAAAAAaaaAAAAAA");
				player.add(pemainBaru);
				hasil = chara + " ditambah ke game";
			}
		}
        return hasil;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        String hasil = "";
		if(find(chara) != null){
			hasil = "Sudah ada karakter bernama " + chara;
		} else{
			Monster pemainBaru = new Monster(chara, hp, roar);
			player.add(pemainBaru);
			hasil = chara + " ditambah ke game";
		}
        return hasil;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
		String hasil = "";
		if(find(chara) == null){
			hasil = "Tidak ada " + chara;
		} else{
			for(Player pemain : player){
				if(pemain.getName().equals(chara)){
					player.remove(pemain);
					hasil = chara + " dihapus dari games";
					break;
				}
			}
		}
        return hasil;
    }

    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
		String hasil = "";
		if(find(chara) == null){
			hasil = "Tidak ada " + chara;
		} else{
			hasil = find(chara).status();
		}
        return hasil;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
		String hasil = "";
		for(Player pemain : player){
			hasil += pemain.status() + "\n";
		}
        return hasil;        
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
		String hasil = "";
		if(find(chara) == null){
			hasil = "Tidak ada " + chara;
		} else{
			hasil = find(chara).getDiet();
		}
        return hasil;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
		String hasil = "";
		for(Player pemain : player){
			hasil += pemain.getName() + ": " + pemain.getDiet() + "\n";
		}
        return hasil;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
		String hasil = "";
		Player penyerang = find(meName);
		Player diserang = find(enemyName);
		if(penyerang.equals(null) || diserang.equals(null)){
			hasil = "Tidak ada " + meName + " atau " + enemyName;
		} else {
			hasil = penyerang.attack(diserang);
		}
        return hasil;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        String hasil = "";
		Player penyerang = find(meName);
		Player diserang = find(enemyName);
		if(penyerang.equals(null) || diserang.equals(null)){
			hasil = "Tidak ada " + meName + " atau " + enemyName;
		} else{
			hasil = penyerang.burn(diserang);
		}
        return hasil;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        String hasil = "";
		Player penyerang = find(meName);
		Player diserang = find(enemyName);
		if(penyerang == null || diserang == null){
			hasil = "Tidak ada " + meName + " atau " + enemyName;
		} else {
			hasil = penyerang.eat(diserang);
			if(penyerang.canEat(diserang)){
				player.remove(diserang);
			}
		}
        return hasil;
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
		String hasil = "";
		if(find(meName) == null){
			hasil = "Tidak ada " + meName;
		} else{
			Player pemain = find(meName);
			if(pemain.getType().equals("Monster")){
				hasil = pemain.getRoar();
			} else{
				hasil = meName + " tidak bisa berteriak";
			}
		}
        return hasil;
    }
}