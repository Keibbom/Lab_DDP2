package character;

public class Magician extends Player{
	//Konstruktor
	public Magician(String name, int hp){
		super(name, "Magician", hp);
	}
	
	//Behavior
	public String burn(Player target){
		if(target.getType().equals("Magician")){
			target.setHp(target.getHp() - 20);
		} else{
			target.setHp(target.getHp() - 10);
		}
		if(target.getHp() == 0){
			target.setMatang(true);
			return "Nyawa " + target.getName() + " 0\n" + "dan matang";
		} else{
		return "Nyawa " + target.getName() + " " + target.getHp();
		}
	}
}
