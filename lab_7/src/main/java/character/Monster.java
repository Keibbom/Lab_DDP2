package character;

public class Monster extends Player{
	//Konstruktor
	public Monster(String name, int hp){
		super(name, "Monster", hp*2, "AAAAAAaaaAAAAAaaaAAAAAA");
	}
	public Monster(String name, int hp, String roar){
		super(name, "Monster", hp*2, roar);
	}
	
	//Behavior
	public String roar(){
		return this.getRoar();
	}
}