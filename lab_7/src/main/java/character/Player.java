package character;

import java.util.ArrayList;

public class Player{
	//Atribut
	private String name;
	private String type;
	private int hp;
	private String roar;
	private boolean matang;
	private ArrayList<Player> diet;

	//Konstruktor
	public Player(String name, String type, int hp){
		this.name = name;
		this.type = type;
		this.hp = hp;
		this.matang = false;
		this.diet = new ArrayList<Player>();
	}
	public Player(String name, String type, int hp, String roar){
		this.name = name;
		this.type = type;
		this.hp = hp;
		this.roar = roar;
		this.matang = false;
		this.diet = new ArrayList<Player>();
	}
	
	//Accessor Mutator
	public String getName(){
		return this.name;
	}
	public String getType(){
		return this.type;
	}
	public int getHp(){
		return this.hp;
	} public void setHp(int hp){
		this.hp = hp;
		if(this.hp < 0){
			this.hp = 0;
		}
	}
	public String getRoar(){
		return this.roar;
	}
	public boolean getMatang(){
		return this.matang;
	} public void setMatang(boolean matang){
		this.matang = matang;
	}
	public String getDiet(){
		String hasil = "";
		if(this.diet.size() == 0){
			hasil = this.name + " belum memakan siapa siapa";
		} else{
			for (Player pemain : this.diet){
				hasil += pemain.type + " " + pemain.name;
			}
		}
		return hasil;
	} public void setDiet(Player target){
		this.diet.add(target);
	}
	
	//Behavior
	public boolean canEat(Player target){
		boolean hasil = false;
		if(this.type.equals("Human") || this.type.equals("Magician")){
			if(target.getType().equals("Monster")){
				if(target.getMatang() == true){
					hasil = true;
				}
			}
		} else{
			if(target.getHp() == 0){
				hasil = true;
			}
		}
		return hasil;
	}	
	public String attack(Player target){
		if(target.getType().equals("Magician")){
			target.setHp(target.getHp() - 20);
		} else{
			target.setHp(target.getHp() - 20);
		}
		return "Nyawa " + target.getName() + " " + target.getHp();
	}
	public String burn(Player target){
		return this.burn(target);
	}
	public String eat(Player target){
		String hasil = "";
		if(this.canEat(target)){
			this.hp += 15;
			this.diet.add(target);
			hasil = this.name + " memakan " + target.getName() + "\n" +
					"Nyawa " + this.name + " kini " + this.hp;
		} else {
			hasil = this.name + " tidak bisa memakan " + target.getName();
		}
		return hasil;
	}
	public String status(){
		String nyawaNya;
		String dietNya;
		if(this.hp == 0){
			nyawaNya = "Sudah meninggal dunia dengan damai";
		} else{
			nyawaNya = "Masih hidup";
		}
		if(this.diet.size() == 0){
			dietNya = "Belum memakan siapa siapa";
		} else{
			dietNya = "Memakan " + this.getDiet();
		}
		return this.type + " " + this.name + "\n" +
				"HP: " + this.hp + "\n" +
				nyawaNya + "\n" +
				dietNya;
	}
}