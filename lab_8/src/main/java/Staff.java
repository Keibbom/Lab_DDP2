public class Staff extends Karyawan{
	//Atribut
    private Karyawan[] bawahan = new Karyawan[10];
    private int jumlahBawahan = 0;
    
	//Konstruktor
    public Staff(String nama, int gaji){
        this.nama = nama;
        this.jabatan = "STAFF";
        this.gaji = gaji;
    }
	
	//Method Penambah Bawahan
    public String tambahBawahan(Karyawan bawahanBaru){
		String hasil = "";
        for (int i = 0; i<10; i++){
            if (bawahan[i] == null){
				break;
			} else if (bawahanBaru.getNama().equals(bawahan[i].getNama())){
                hasil = "Karyawan " + bawahanBaru.getNama() + " telah menjadi bawahan " + this.nama;
				break;
            }
        }
		if(hasil.equals("")){
			if(bawahanBaru instanceof Intern){
				bawahan[jumlahBawahan] = bawahanBaru;
				jumlahBawahan += 1;
				hasil = "Karyawan " + bawahanBaru.getNama() + " berhasil ditambahkan menjadi bawahan " + this.nama;
			}
		}
        return hasil;
    }
}
