import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class Lab8{
    public static void main(String[] args){
		//Inisiasi Objek Korporasi dan File Inputan
        Korporasi korporasi = new Korporasi();
        File text = new File("input.txt");
		
        try{
            Scanner input = new Scanner(text);
			
			//Set Gaji Maksimal Staff
			korporasi.setGajiMaksimal(Integer.parseInt(input.nextLine()));
			
			//Loop selama File Masih Ada Isinya
            while (input.hasNextLine()){
                String[] perintah = input.nextLine().split(" ");
                
				//Menjalankan Method Sesuai Inputan Perintah
				if(perintah[0].equals("TAMBAH_KARYAWAN")){
					korporasi.tambahKaryawan(perintah[1], perintah[2], Integer.parseInt(perintah[3]));
				} else if(perintah[0].equals("STATUS")){
					korporasi.status(perintah[1]);
				} else if(perintah[0].equals("TAMBAH_BAWAHAN")){
					korporasi.tambahBawahan(perintah[1],perintah[2]);
                } else if (perintah[0].equals("GAJIAN")){
					korporasi.gajian();
                } else{
                    System.out.println("Maaf, Perintah Salah! Silakan Coba Lagi!");
                }
            }
        }
        catch(FileNotFoundException e){
            System.out.println("Maaf, File Tidak Ditemukan!");
        }
    }
}
