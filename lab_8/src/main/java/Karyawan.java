abstract class Karyawan{
	//Atribut
    protected String nama;
    protected String jabatan;
    protected int gaji;
    protected int jumlahGaji = 0;

    //Accessor
    public String getNama(){
        return this.nama;
    }
    public int getGaji(){
        return this.gaji;
    }

    // Method Gajian
    public void gajian(){
        jumlahGaji += 1;
        if(jumlahGaji % 6 == 0){
			tambahGaji();
		}
    }
    public void tambahGaji(){
        int gajiBaru = gaji*11/10;
        System.out.println(this.nama + " mengalami kenaikan gaji sebesar 10% dari " + this.gaji + " menjadi " + gajiBaru);
        this.gaji = gajiBaru;
    }
}
