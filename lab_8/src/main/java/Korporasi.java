import java.util.ArrayList;

public class Korporasi{
	//Atribut
    private ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();
	public int gajiMaksimal;
	
	//Set Gaji Maksimum
	public void setGajiMaksimal(int uang){
		this.gajiMaksimal = uang;
	}
	
	//Mencari Karyawan
    public Karyawan cariKaryawan(String nama){
		Karyawan hasil = null;
        for (Karyawan i : karyawan){
            if (i.getNama().equals(nama)){
				hasil = i;
			}
        }
        return hasil;
	}
	
	//Menambah Karyawan
	public void tambahKaryawan(String nama, String jabatan, int gaji){
		if(cariKaryawan(nama) != null){
			System.out.println("Karyawan dengan nama " + nama + " telah terdaftar");
		} else{
			if(karyawan.size() < 10000){
				if(jabatan.equals("MANAGER")){
					karyawan.add(new Manager(nama,gaji));
				} else if(jabatan.equals("STAFF")){
					karyawan.add(new Staff(nama,gaji));
				} else{
					karyawan.add(new Intern(nama,gaji));
				}
				System.out.println(nama + " mulai bekerja sebagai " + jabatan + " di PT. TAMPAN");
			} else{
				System.out.println("PT. TAMPAN tidak bisa menambah karyawan baru lagi");
			}
		}
	}
	
	//Status Karyawan
	public void status(String nama){
	 	if(cariKaryawan(nama) == null){
	 		System.out.println("Karyawan tidak ditemukan");
	 	} else{
			System.out.println(cariKaryawan(nama).getNama() + " " + cariKaryawan(nama).getGaji());
		}
    }
    
	//Menggaji Karyawan
	public void gajian(){
        for (Karyawan i : karyawan){
            i.gajian();
            if (i instanceof Staff & i.getGaji() > gajiMaksimal){
                i = new Manager(i.getNama(),i.getGaji());
                System.out.println("Selamat, " + i.getNama() + " telah dipromosikan menjadi MANAGER");
            }
        }
		System.out.println("Semua karyawan telah diberikan gaji");
	}
	
	//Menambah Bawahan
	public void tambahBawahan(String merekrut, String direkrut){
		Karyawan atasan = cariKaryawan(merekrut);
		Karyawan bawahan = cariKaryawan(direkrut);
		if(atasan == null | bawahan == null){
			System.out.println("Nama tidak berhasil ditemukan");
		} else if(atasan instanceof Manager){
			System.out.println(((Manager)atasan).tambahBawahan(bawahan));
		} else if(atasan instanceof Staff){
			System.out.println(((Staff)atasan).tambahBawahan(bawahan));
		} else{
			System.out.println("Anda tidak layak memiliki bawahan");
		}
	}
}
