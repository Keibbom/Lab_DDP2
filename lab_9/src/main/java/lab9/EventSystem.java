package lab9;

import lab9.user.User;
import lab9.event.Event;
import java.util.Calendar;
import java.util.ArrayList;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class EventSystem{
    //Atribut
    private ArrayList<Event> events;
    private ArrayList<User> users;
    
    //Konstruktor
    public EventSystem(){
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    
	//Method
	public User getUser(String name){
		User hasil = null;
		for(User i: users){
			if(i.getName().equals(name)){
				hasil = i;
			}
		}
		return hasil;
	}
	public Event cariEvent(String name){
		Event hasil = null;
		for(Event i: events){
			if(i.getName().equals(name)){
				hasil = i;
			}
		}
		return hasil;
	}
	public String addUser(String name){
		String hasil = "";
		if(getUser(name) != null){
			hasil = "User " + name + " sudah ada!";
		} else{
			User pesertaBaru = new User(name);
			users.add(pesertaBaru);
			hasil = "User " + name + " berhasil ditambahkan!";
		}
		return hasil;
    }
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr){
		String hasil = "";
		if(cariEvent(name) != null){
			hasil = "Event " + name + " sudah ada!";
		} else{
			SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Calendar startTime = Calendar.getInstance();
            Calendar endTime = Calendar.getInstance();
			try{
				startTime.setTime(time.parse(startTimeStr));
            } catch (ParseException e){
                e.printStackTrace();
            }
            try{
                endTime.setTime(time.parse(endTimeStr));
            } catch (ParseException e) {
                e.printStackTrace();
            }
			if(endTime.before(startTime)){
                hasil = "Waktu yang diinputkan tidak valid!";
			} else{
				BigInteger cost = new BigInteger(costPerHourStr);
				Event eventBaru = new Event(name, startTime, endTime, cost);
				events.add(eventBaru);
				hasil = "Event " + name + " berhasil ditambahkan!";
			}
		}
		return hasil;
    }
	public String getEvent(String name){
		String hasil = "Tidak ada acara dengan nama " + name + "!";
		for(Event i: events){
			if(i.getName().equals(name)){
				hasil = i.toString();
			}
		}
		return hasil;
	}
    public String registerToEvent(String userName, String eventName){
		String hasil = "";
		User userNya = getUser(userName);
		Event eventNya = cariEvent(eventName);
		if(userNya == null || eventNya == null){
			if(eventNya != null){
				hasil = "Tidak ada pengguna dengan nama " + userName + "!";
			} else if(userNya != null){
				hasil = "Tidak ada acara dengan nama " + eventName + "!";
			} else{
				hasil = "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
			}
		} else{
			if(userNya.addEvent(eventNya)){
				hasil = userName + " berencana menghadiri " + eventName + "!";          
			} else{
				hasil = userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
			}
		}
        return hasil;
    }
}