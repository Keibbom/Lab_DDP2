package lab9.event;

import java.util.Calendar;
import java.math.BigInteger;
import java.text.SimpleDateFormat;

public class Event{
    //Atribut
    private String name;
	private Calendar startTime;
	private Calendar endTime;
	private BigInteger cost;
	
	//Konstruktor
	public Event(String name, Calendar startTime, Calendar endTime, BigInteger cost){
		this.name = name;
		this.startTime = startTime;
		this.endTime = endTime;
		this.cost = cost;
    }
    
    //Method
    public String getName(){
        return this.name;
    }
	public Calendar getStartTime(){
		return this.startTime;
	}
	public Calendar getEndTime(){
		return this.endTime;
	}
	public BigInteger getCost(){
		return this.cost;
	}
	public String toString(){
		SimpleDateFormat time = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
		return this.name + "\n" +
			"Waktu mulai: " + time.format(startTime.getTime()) + "\n" +
			"Waktu selesai: " + time.format(endTime.getTime()) + "\n" +
			"Biaya kehadiran: " + this.cost;
	}
}
