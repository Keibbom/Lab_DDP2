package lab9.user;

import lab9.event.Event;
import java.util.ArrayList;
import java.math.BigInteger;

public class User{
    //Atribut
    private String name;
    private ArrayList<Event> events;
    
    //Konstruktor
    public User(String name){
        this.name = name;
        this.events = new ArrayList<>();
    }
    
    //Method
    public String getName(){
        return name;
    }
    public boolean addEvent(Event newEvent){
		boolean bisaTambah = true;
		for(Event i : events){
			if(newEvent.getStartTime().before(i.getEndTime()) && 
				newEvent.getEndTime().after(i.getStartTime())){
					bisaTambah = false;
			}
		}
		if(bisaTambah){
			events.add(newEvent);
		}
        return bisaTambah;
    }
    public ArrayList<Event> getEvents(){
		ArrayList<Event> eventsNya = events;
        return eventsNya;
    }
	public BigInteger getTotalCost(){
		BigInteger hasil = new BigInteger("0");
		for(Event i : events){
			hasil = hasil.add(i.getCost());
		}
		return hasil;
	}
}
