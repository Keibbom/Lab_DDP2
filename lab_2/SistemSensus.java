import java.util.Scanner;

public class SistemSensus {
	public static void main(String[] args) {
		try{
			// Input Scanner Baru
			Scanner input = new Scanner(System.in);

			// Inputan Data User
			System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
					"--------------------\n" +
					"Nama Kepala Keluarga   : ");
			String nama = input.nextLine();
			
			System.out.print("Alamat Rumah           : ");
			String alamat = input.nextLine();
			
			System.out.print("Panjang Tubuh (cm)     : ");
			String panjang = input.nextLine();
			short panjangSrt = Short.parseShort(panjang);
			if(panjangSrt>250 || panjangSrt<=0){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!" + "\n");
				System.exit(0);
			}
			
			System.out.print("Lebar Tubuh (cm)       : ");
			String lebar = input.nextLine();
			short lebarSrt = Short.parseShort(lebar);
			if(lebarSrt>250 || lebarSrt<=0){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!" + "\n");
				System.exit(0);
			}
			
			System.out.print("Tinggi Tubuh (cm)      : ");
			String tinggi = input.nextLine();
			short tinggiSrt = Short.parseShort(tinggi);
			if(tinggiSrt>250 || tinggiSrt<=0){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!" + "\n");
				System.exit(0);
			}
			
			System.out.print("Berat Tubuh (kg)       : ");
			String berat = input.nextLine();
			float beratFlt = Float.parseFloat(berat);
			if(beratFlt>250 || beratFlt<=0){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!" + "\n");
				System.exit(0);
			}
			
			System.out.print("Jumlah Anggota Keluarga: ");
			String jumlah = input.nextLine();
			short jumlahSrt = Short.parseShort(jumlah);
			if(jumlahSrt>20 || jumlahSrt<=0){
				System.out.print("WARNING: Keluarga ini tidak perlu direlokasi!" + "\n");
				System.exit(0);
			}
			
			System.out.print("Tanggal Lahir          : ");
			String tanggalLahir = input.nextLine();
			
			System.out.print("Catatan Tambahan       : ");
			String catatan = input.nextLine();
			
			System.out.print("Jumlah Cetakan Data    : ");
			String jumlahCetakan = input.nextLine();
			short jumlahCetakanSrt = Short.parseShort(jumlahCetakan);
			
			System.out.println("\n");

			// Menghitung Rasio
			float rasio = beratFlt/ (((float)(panjangSrt*lebarSrt*tinggiSrt)) / 1000000);
			short rasioSrt = (short) rasio;
			String rasioStr = String.valueOf(rasioSrt);

			//Mencetak Data
			for (short i = 0; i < (jumlahCetakanSrt); i++) {
				//Input Nama Penerima
				System.out.print("Pencetakan " + String.valueOf(i+1) + " dari " + String.valueOf(jumlahCetakan) + " untuk: ");
				String penerima = input.nextLine(); 
				System.out.println("DATA SIAP DICETAK UNTUK " + penerima.toUpperCase() + "\n" + 
						"--------------------\n" +
						nama + " - " + alamat + "\n" +
						"Lahir pada tanggal " + tanggalLahir + "\n" +
						"Rasio Berat Per Volume	= " + rasioStr + " kg/m^3");

				// Print Catatan
				String catatanFix = "";
				if (catatan.length() > 0){
					catatanFix = "Catatan: " + catatan;
				} else {
					catatanFix = "Tidak ada catatan tambahan";
				}
				System.out.println(catatanFix + "\n");
			}
			
			//Membuat Nomor Keluarga
			char hurufPertama = nama.charAt(0);
			short karakter = 0;
			for (char i: nama.toCharArray()) {
				karakter += ((short) i);
			}
			long nomorLng = ((panjangSrt*lebarSrt*tinggiSrt)+karakter) % 10000;
			String nomorKeluarga = Character.toString(hurufPertama) + String.valueOf(nomorLng);
			
			//Menghitung Anggaran Makanan per tahun
			long anggaran = 50000*365*jumlahSrt;
			
			//Menghitung Umur
			String tanggalSplit[] = tanggalLahir.split("-");
			String tahunLahir = tanggalSplit[2];
			short tahunLahirSrt = Short.parseShort(tahunLahir);
			long umur = 2018 - tahunLahirSrt;
			
			//Rekomendasi Apartemen
			String apartemen = "";
			if(umur<19){
				apartemen = "PPMT, kabupaten Rotunda";
			}else if(anggaran<100000000){
				apartemen = "Teksas, kabupaten Sastra";
			}else{
				apartemen = "Mares, kabupaten Margonda";
			}
			System.out.println("REKOMENDASI APARTEMEN\n" +
				"--------------------\n" +
				"MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga + "\n" +
				"MENIMBANG:  Anggaran makanan tahunan: Rp " + String.valueOf(anggaran) + "\n" +
				"            Umur kepala keluarga: " + String.valueOf(umur) + " tahun\n" +
				"MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" +
				apartemen);
			
			input.close();
		}catch(Exception e){
			System.out.println("Mohon masukkan data yang valid!");
		}
	}
}