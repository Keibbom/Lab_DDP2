//Objek Angka

public class Number {

	//Atribut-Atributnya
	private int value;
	private int x;
	private int y;
	private boolean isChecked;

	//Inisiasi Objek Menggunakan 3 Parameter
	public Number(int value, int x, int y) {
		super();
		this.value = value;
		this.x = x;
		this.y = y;
	}

	//Setter dan Getter untuk Nilai, Koordinat X dan Y, serta Status Angka
	public void setValue(int value) {
		this.value = value;
	}
	public int getValue() {
		return value;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getX() {
		return x;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getY() {
		return y;
	}
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}
	public boolean isChecked() {
		return isChecked;
	}
}
