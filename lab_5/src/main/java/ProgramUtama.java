import java.util.Scanner;

//Program Utama untuk Permainan Bingo
public class ProgramUtama{
	public static void main(String[] args){
	
		//Try Apabila User Tidak Menginput 25 Angka atau Menginput selain Bilangan
		try{
			//Tampilan Awal
			System.out.println("WELCOME TO BINGO!\n" +
						"--------------------\n" +
						"Please insert your 25 card :");
			Scanner input = new Scanner(System.in);
			
			//Meminta Inputan Sebanyak 25 Kali lalu Dijadikan Array
			String[][] inputanSplit = new String[5][5];
			for(int i=0; i<5; i++){
				String inputan = input.nextLine();
				inputanSplit[i] = inputan.split(" ");
			}
			
			//Array yang Berisi Angka dari User dijadikan Objek Number
			Number[][] numbers = new Number[5][5];
			Number[] numberStates = new Number[100];
			for(int i=0; i<5; i++){
				for(int j=0; j<5; j++){
					int nilai = Integer.parseInt(inputanSplit[i][j]);
					numbers[i][j] = numberStates[nilai] = new Number(nilai, i, j);
				}
			}
			
			//Membuat Objek BingoCard
			BingoCard kartu = new BingoCard(numbers, numberStates);
			
			//Meminta Inputan Perintah Selama Belum Menang
			System.out.println("\nPlease insert the command :");
			while(kartu.menang() == false){
				String inputan2 = input.nextLine();
				String[] inputan2Split = inputan2.split(" ");
				String perintah = inputan2Split[0];
				
				//Perintah untuk Menyilang Angka
				if(perintah.equals("MARK")){
					int angka = Integer.parseInt(inputan2Split[1]);
					System.out.println(kartu.markNum(angka));
				}
				
				//Perintah untuk Merestart Papan Bingo
				else if(perintah.equals("RESTART")){
					kartu.restart();
				} 
				
				//Perintah untuk Menampilkan Papan Bingo
				else if(perintah.equals("INFO")){
					System.out.println(kartu.info());
				} 
				
				//Jika Perintah Tidak Valid
				else{
					System.out.println("Incorrect command\n");
				}
				System.out.println("");
			}
		}
		
		//Ketika User Tidak Menginput 25 Angka atau Menginput selain Bilangan
		catch(Exception e){
			System.out.println("Please insert 25 numbers!");
		}
	}
}