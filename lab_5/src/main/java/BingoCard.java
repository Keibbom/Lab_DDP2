//Objek Papan Bingo
public class BingoCard {

	//Atribut-Atributnya
	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	//Inisiasi Objek Menggunakan 2 Parameter
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}
	
	//Setter dan Getter untuk Angka, Lokasi Angka, dan Status Bingo
	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}
	public Number[][] getNumbers() {
		return numbers;
	}
	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	
	public Number[] getNumberStates() {
		return numberStates;
	}
	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}
	public boolean isBingo() {
		return isBingo;
	}

	//Method untuk Menyilang Angka
	public String markNum(int num){
		if(numberStates[num] == null){
			return "Kartu tidak memiliki angka " + num;
		} else{
			if(numberStates[num].isChecked()){
				return num + " sebelumnya sudah tersilang";
			} else{
				numberStates[num].setChecked(true);
				if(this.menang() == true){
					this.setBingo(true);
					return num + " tersilang\n\nBINGO\n" + this.info();
				} else{
					return num + " tersilang";
				}
			}
		}
	}	
	
	//Method untuk Mencetak Kondisi Kartu Saat Ini
	public String info(){
		String informasi = "";
		for(int i = 0; i < 5; i++){
			for(int j = 0; j < 5; j++){
				if(numbers[i][j].isChecked()){
					if(i==4 && j==4){
						informasi += "| X  |";
					} else if(j==4){
						informasi += "| X  |\n";
					} else{
						informasi += "| X  ";
					}
				} else{
					if(i==4 && j==4){
						informasi += "| " + numbers[i][j].getValue() + " |";
					} else if(j==4){
						informasi += "| " + numbers[i][j].getValue() + " |\n";
					} else{
						informasi += "| " + numbers[i][j].getValue() + " ";
					}
				}
			}
		}
		return informasi;
	}
	
	//Method untuk Menghilangkan Seluruh Coretan Pada Angka
	public void restart(){
		for(int i = 0; i < 5; i++){
			for(int j = 0; j < 5; j++){
				numbers[i][j].setChecked(false);
			}
		}
		System.out.println("Mulligan!\n");
	}
	
	//Method untuk Mengecek Apakah Sudah Bingo atau Belum
	public boolean menang(){
		if(numbers[0][0].isChecked() == true &&
			numbers[1][1].isChecked() == true &&
			numbers[2][2].isChecked() == true &&
			numbers[3][3].isChecked() == true &&
			numbers[4][4].isChecked() == true){
			return true;
		} else if(numbers[0][4].isChecked() == true &&
			numbers[1][3].isChecked() == true &&
			numbers[2][2].isChecked() == true &&
			numbers[3][1].isChecked() == true &&
			numbers[4][0].isChecked() == true){
			return true;
		} else{
			for(int i = 0; i<5; i++){
				if(numbers[i][0].isChecked() == true &&
					numbers[i][1].isChecked() == true &&
					numbers[i][2].isChecked() == true &&
					numbers[i][3].isChecked() == true &&
					numbers[i][4].isChecked() == true){
					return true;
					
				} else if(numbers[0][i].isChecked() == true &&
					numbers[1][i].isChecked() == true &&
					numbers[2][i].isChecked() == true &&
					numbers[3][i].isChecked() == true &&
					numbers[4][i].isChecked() == true){
					return true;
				}
			}
		} 
		return false;
	}
	
}
