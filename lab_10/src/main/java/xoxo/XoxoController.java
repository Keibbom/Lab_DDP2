package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Paths;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        //TODO: Write your code for logic and everything here
        gui.getFrame().setPreferredSize(new Dimension(370,370));
        gui.getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.getFrame().pack();
        gui.getFrame().setVisible(true);

        gui.setDecryptFunction(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String key = gui.getKeyText();
                int seed = 0;
                String message = gui.getMessageText();
                String result = "";

                try {
                    seed = Integer.parseInt(gui.getSeedText());
                    result = new XoxoDecryption(key).decrypt(message,seed);
                } catch (NumberFormatException e){
                    result = new XoxoDecryption(key).decrypt(message);
                }

                try {
                    gui.appendLog(writeFile(result, "txt"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        gui.setEncryptFunction(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                String key = gui.getKeyText();
                int seed = 0;
                String message = gui.getMessageText();
                String result = "";

                try {
                    seed = Integer.parseInt(gui.getSeedText());
                    result = new XoxoEncryption(key).encrypt(message,seed).getEncryptedMessage();
                } catch (NumberFormatException e){
                    result = new XoxoEncryption(key).encrypt(message).getEncryptedMessage();
                }

                try {
                    gui.appendLog(writeFile(result, "enc"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //TODO: Create any methods that you want
    public String writeFile(String message, String fileType) throws IOException {
        String path = Paths.get("").toAbsolutePath().toString()+"\\"+"output."+fileType;
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(path, true));
            writer.write(message);
            writer.newLine();
            return "file successfully created at: "+path;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
        return "file creation failed";
    }
}