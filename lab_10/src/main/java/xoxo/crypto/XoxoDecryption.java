package xoxo.crypto;

import xoxo.key.HugKey;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    public String decrypt(String encryptedMessage){
        return decrypt(encryptedMessage, HugKey.DEFAULT_SEED);
    }
    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        //TODO: Implement decryption algorithm
        String finale = "";
        for(int i = 0; i < encryptedMessage.length(); i++){
            int temp = hugKeyString.charAt(i % hugKeyString.length()) ^ seed;
            temp -= (int)'a';
            temp ^= encryptedMessage.charAt(i);
            finale += (char)temp;
        }
        return finale;
    }
}