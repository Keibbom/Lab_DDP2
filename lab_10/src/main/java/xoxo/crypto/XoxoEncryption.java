package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException{
        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if seed is not in range of HugKey range
     */
    public XoxoMessage encrypt(String message, int seed) throws RangeExceededException{
        //TODO: throw RangeExceededException for seed requirements
        String encryptedMessage = this.encryptMessage(message);
        if (seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE){
            throw new RangeExceededException("seed value is not in range 0-36");
        }
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if length of the message is greater than 1250 Byte or 10 Kbit
     * @throws InvalidCharacterException if kiss key character is not a-z or A-Z or @ or space
     */
    private String encryptMessage(String message) throws SizeTooBigException, InvalidCharacterException{
        //TODO: throw SizeTooBigException for message requirements

        final int length = message.length();
        if(length > 1250){
            throw new SizeTooBigException("size exceeds 10 Kbit");
        }
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {
            int m = message.charAt(i);
            //TODO: throw InvalidCharacterException for message requirements
            if(!(""+(char)this.kissKey.keyAt(i)).matches("[a-zA-Z@ ]")){
                throw new InvalidCharacterException("char is invalid");
            }
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }
}