package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {
    private JFrame frame;
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file successfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    public JFrame getFrame() {
        return frame;
    }

    private JPanel createPanel(){
        JPanel container = new JPanel(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.WEST;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 0;
        constraints.gridwidth = 1;
        constraints.insets = new Insets(0, 10, 0, 40);
        container.add(new JLabel("Key"), constraints);

        constraints.gridy = 1;
        container.add(new JLabel("Seed"), constraints);

        constraints.gridy = 2;
        container.add(new JLabel("Message"), constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.weightx = 3;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.insets = new Insets(5, 0, 0, 15);
        container.add(keyField, constraints);

        constraints.gridy = 1;
        container.add(seedField, constraints);

        constraints.gridy = 2;
        constraints.ipady = 100;
        container.add(messageField, constraints);

        JPanel buttonPanel = new JPanel(new GridBagLayout());
        GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.fill = GridBagConstraints.HORIZONTAL;
        buttonPanel.add(encryptButton, buttonConstraints);
        buttonConstraints.gridx = 1;
        buttonPanel.add(decryptButton, buttonConstraints);

        constraints.gridy = 3;
        constraints.ipady = 3;
        container.add(buttonPanel, constraints);

        constraints.gridy = 4;
        constraints.gridx = 0;
        constraints.gridwidth = 2;
        constraints.insets.left = 15;
        constraints.insets.top = 30;
        constraints.ipady = 50;
        JScrollPane logScroll = new JScrollPane(logField);
        container.add(logScroll, constraints);
        return container;
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        frame = new JFrame("Hidden Leaf Message");
        messageField = new JTextField();
        keyField = new JTextField();
        seedField = new JTextField();
        encryptButton = new JButton("encrypt");
        decryptButton = new JButton("decrypt");
        logField = new JTextArea();
        frame.add(createPanel());
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}