//INI MERUPAKAN CLASS MANUSIA
public class Manusia{
	//Atribut dari Kelas Manusia
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	
	//Constructor Overloading
	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
		this.uang = 50000;
		this.kebahagiaan = 50;
	}
	public Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
	}
	
	//Setter and Getter
	public void setNama(String nama){
		this.nama = nama;
	}
	public String getNama(){
		return nama;
	}
	public void setUmur(int umur){
		this.umur = umur;
	}
	public int getUmur(){
		return umur;
	}
	public void setUang(int uang){
		this.uang = uang;
	}
	public int getUang(){
		return uang;
	}
	public void setKebahagiaan(float kebahagiaan){
		this.kebahagiaan = kebahagiaan;
	}
	public float getKebahagiaan(){
		return kebahagiaan;
	}
	
	//Method untuk Mentransfer Uang
	public void beriUang(Manusia penerima){
		int asciiPenerima = 0;
		String namaPenerima = penerima.getNama();
		for (char i: namaPenerima.toCharArray()) {
				asciiPenerima += ((int) i);
			}
		int jumlahUang = asciiPenerima * 100;
		
		if (jumlahUang <= this.uang) {
			this.uang -= jumlahUang;
			penerima.setUang(penerima.getUang() + jumlahUang);
			this.kebahagiaan += (((float)jumlahUang) / 6000);
			penerima.setKebahagiaan(penerima.getKebahagiaan() + (((float)jumlahUang) / 6000));
			if (this.kebahagiaan > 100){
				this.kebahagiaan = 100;
			}
			if (penerima.getKebahagiaan() > 100){
				penerima.setKebahagiaan(100);
			}
			System.out.println(nama + " memberi uang sebanyak " + jumlahUang + 
				" kepada " + penerima.getNama() + ", mereka berdua senang :D");
		}	else{
			System.out.println(nama + " ingin memberi uang kepada " + penerima.getNama() +
				" namun tidak memiliki cukup uang :'(");
		}
	}
	
	//Method Overloading dalam Mentransfer Uang
	public void beriUang(Manusia penerima, int jumlahUang){
		if (jumlahUang <= this.uang) {
			this.uang -= jumlahUang;
			penerima.setUang(penerima.getUang() + jumlahUang);
			this.kebahagiaan += (((float)jumlahUang) / 6000);
			penerima.setKebahagiaan(penerima.getKebahagiaan() + (((float)jumlahUang) / 6000));
			if (this.kebahagiaan > 100){
				this.kebahagiaan = 100;
			}
			if (penerima.getKebahagiaan() > 100){
				penerima.setKebahagiaan(100);
			}
			System.out.println(nama + " memberi uang sebanyak " + jumlahUang + 
				" kepada " + penerima.getNama() + ", mereka berdua senang :D");
		}	else{
			System.out.println(nama + " ingin memberi uang kepada " + penerima.getNama() +
				" namun tidak memiliki cukup uang :'(");
		}
	}
	
	//Method untuk Bekerja
	public void bekerja(int durasi, int bebanKerja){
		if (umur < 18){
			System.out.println(nama + " belum boleh bekerja karena masih dibawah umur D:");
		} else{
			int bebanKerjaTotal = durasi * bebanKerja;
			if (bebanKerjaTotal <= kebahagiaan){
				kebahagiaan -= (float)bebanKerjaTotal;
				if (this.kebahagiaan < 0){
					this.kebahagiaan = 0;
				}
				int pendapatan = bebanKerjaTotal * 10000;
				uang += pendapatan;
				System.out.println(nama + " bekerja full time, total pendapatan : " + pendapatan);
			} else {
				int durasiBaru = (int)kebahagiaan / bebanKerja;
				bebanKerjaTotal = durasiBaru * bebanKerja;
				kebahagiaan -= (float)bebanKerjaTotal;
				int pendapatan = bebanKerjaTotal * 10000;
				uang += pendapatan;
				System.out.println(nama + " tidak bekerja secara full time karena sudah terlalu lelah," +
					" total pendapatan : " + pendapatan);
			}
		}
	}
	
	//Method untuk Berekreasi
	public void rekreasi(String tempat){
		int biaya = tempat.length() * 10000 ;
		if (uang < biaya) {
			System.out.println(nama + " tidak mempunyai cukup uang untuk berekreasi di " + tempat + " :(");
		} else{
			uang -= biaya;
			kebahagiaan += tempat.length();
			if (this.kebahagiaan > 100){
				this.kebahagiaan = 100;
			}
			System.out.println(nama + " berekreasi di " + tempat + ", " + nama + " senang :)");
		}
	}
	
	//Method ketika Sakit
	public void sakit(String namaPenyakit){
		kebahagiaan -= namaPenyakit.length();
		if (this.kebahagiaan < 0){
			this.kebahagiaan = 0;
		}
		System.out.println(nama + " terkena penyakit " + namaPenyakit + " :0");
	}
	
	//Representasi Object
	public String toString(){
		return "Nama\t\t: " + nama + "\n" +
			"Umur\t\t: " + umur + "\n" +
			"Uang\t\t: " + uang + "\n" +
			"Kebahagiaan\t: " + kebahagiaan;
	}
}