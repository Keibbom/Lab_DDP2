import java.util.Scanner;

public class RabbitHouse {
	//Fungsi untuk Kelinci Normal
	public static int normal(int lenNama){
		if (lenNama == 1) {
			return 1;
		}
		else {
			return 1 + (lenNama * normal(lenNama - 1));
		}
	}
	
	public static void main(String[] args) {
		// Input Scanner Baru
		Scanner input = new Scanner(System.in);

		// Inputan Data User
		System.out.print("PROGRAM PENGHITUNG JUMLAH KELINCI\n" + 
			"--------------------\n" +
			"Jenis dan Nama Kelinci:\n" + 
			">> ");
		String inputan = input.nextLine();
		
		//Mengolah Inputan dan Validasi Inputan
		String inputanSplit[] = inputan.split(" ");
		String spesifikasi = inputanSplit[0];
		String nama = inputanSplit[1];
		if (nama.length() > 10) {
			System.out.print("Jumlah nama melebihi kapasitas!\n");
			System.exit(0);
		}
		
		//Program untuk Kelinci Normal
		if (spesifikasi.equals("normal")) {
			System.out.print("\nJumlah kelinci: \n>> " + 
				normal(nama.length()) + " ekor\n");
		}
		
		//Selain Kelinci Normal
		else {
			System.out.print("Jenis kelinci tidak dapat ditemukan!");
		}
		
	}
}

