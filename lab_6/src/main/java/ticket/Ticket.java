package ticket;

import movie.Movie;

public class Ticket{
	//Atribut
	private Movie film;
	private String jadwal;
	private boolean tigaDimensi;
	private int harga;
	
	//Konstruktor
	public Ticket(Movie film, String jadwal, boolean tigaDimensi){
		this.film = film;
		this.jadwal = jadwal;
		this.tigaDimensi = tigaDimensi;
		this.harga = hargaTiket();
	}
	
	//Mutator & Accessor
	public void setFilm(Movie film){
		this.film = film;
	} public Movie getFilm(){
		return this.film;
	}
	public void setJadwal(String jadwal){
		this.jadwal = jadwal;
	} public String getJadwal(){
		return this.jadwal;
	}
	public void setTigaDimensi(boolean tigaDimensi){
		this.tigaDimensi = tigaDimensi;
	} public boolean getTigaDimensi(){
		return this.tigaDimensi;
	}
	public String getJenis(){
		String jenisNya = "";
		if(tigaDimensi){
			jenisNya = "3 Dimensi";
		} else{
			jenisNya = "Biasa";
		} return jenisNya;
	}
	public int getHarga(){
		return this.harga;
	}
	
	//Method
	public int hargaTiket(){
		int hargaFix = 0;
		if(jadwal.equals("Sabtu") || jadwal.equals("Minggu")){
			hargaFix = 100000;
		} else{
			hargaFix = 60000;
		}
		if(tigaDimensi == true){
			hargaFix += hargaFix/5;
		}
		return hargaFix;
	}
	
	//Informasi
	public String toString(){
		String jenis = "";
		if(tigaDimensi){
			jenis = "3 Dimensi";
		} else{
			jenis = "Biasa";
		}
		return "------------------------------------------------------------------\n" +
			"Film\t\t: " + film.getJudul() + "\n" +
			"Jadwal Tayang\t: " + jadwal + "\n" +
			"Jenis\t: " + jenis + "\n" +
			"------------------------------------------------------------------";
	}
}
	