package movie;

public class Movie{
	//Atribut
	private String judul;
	private String rating;
	private int durasi;
	private String genre;
	private String jenis;
	private int rate;
	
	//Konstruktor
	public Movie(String judul,String rating,int durasi, String genre, String jenis){
		this.judul = judul;
		this.rating = rating;
		this.durasi = durasi;
		this.genre = genre;
		this.jenis = jenis;
		//Rate
		if(this.rating.equals("Remaja")){
			this.rate = 13;
		} else if(this.rating.equals("Dewasa")){
			this.rate = 17;
		} else{
			this.rate = 0;
		}
	}
	
	//Mutator & Accessor
	public void setJudul(String judul){
		this.judul = judul;
	} public String getJudul(){
		return this.judul;
	}
	public void setRating(String rating){
		this.rating = rating;
	} public String getRating(){
		return this.rating;
	}
	public void setDurasi(int durasi){
		this.durasi = durasi;
	} public int getDurasi(){
		return this.durasi;
	}
	public void setGenre(String genre){
		this.genre = genre;
	} public String getGenre(){
		return this.genre;
	}
	public void setJenis(String jenis){
		this.jenis = jenis;
	} public String getJenis(){
		return this.jenis;
	}
	public int getRate(){
		return this.rate;
	}
	
	//Informasi
	public String toString(){
		return "------------------------------------------------------------------\n" +
			"Judul\t: " + judul + "\n" +
			"Genre\t: " + genre + "\n" +
			"Durasi\t: " + durasi + " menit\n" +
			"Rating\t: " + rating + "\n" +
			"jenis\t: Film " + jenis + "\n" +
			"------------------------------------------------------------------";
	}
}
	