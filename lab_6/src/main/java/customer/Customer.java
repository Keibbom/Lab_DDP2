package customer;

import theater.Theater;
import ticket.Ticket;
import movie.Movie;
import java.util.ArrayList;

public class Customer{
	//Atribut
	private String nama;
	private int umur;
	private boolean perempuan;
	private ArrayList<Ticket> punyaTiket;
	private Ticket sudahTonton;
	
	//Konstruktor
	public Customer(String nama, boolean perempuan, int umur){
		this.nama = nama;
		this.perempuan = perempuan;
		this.umur = umur;
		punyaTiket = new ArrayList<Ticket>();
	}
	
	//Method
	public Ticket orderTicket(Theater bioskop, String film, String hari, String jenis){
		boolean tigaDimensi = false;
		if(jenis.equals("3 Dimensi")){
			tigaDimensi = true;
		}
		String rating = "";
		int rate = 0;
		for(Movie films : bioskop.getFilm()){
            if(films.getJudul().equals(film)){
				rate = films.getRate();
				rating = films.getRating();
			}
		}
		Ticket tiketBaru = bioskop.beliTiket(film, hari, tigaDimensi);
		if(tiketBaru == null){
			System.out.println("Tiket untuk film " + film + " jenis " + jenis + " dengan jadwal " + hari + " tidak tersedia di " + bioskop.getNama());
			return null;
		} else if(umur >= rate){
			System.out.println(nama + " telah membeli tiket " + film + " jenis " + jenis + " di " + bioskop.getNama() + " pada hari " + hari +" seharga Rp. " + tiketBaru.getHarga());
			bioskop.setSaldo(bioskop.getSaldo() + tiketBaru.getHarga());
			bioskop.pendapatanTotal += tiketBaru.getHarga();
			punyaTiket.add(tiketBaru);
			return tiketBaru;
		} else{
			System.out.println(nama + " masih belum cukup umur untuk menonton " + film + " dengan rating " + rating);
			return null;
		}
	}
	public void findMovie(Theater bioskop, String film){
		if(contains(bioskop.getFilm(), film)){
			for(Movie films : bioskop.getFilm()){
				if(films.getJudul().equals(film)){
					System.out.println(films.toString());
				}
			}
		} else{
			System.out.println("Film " + film + " yang dicari " + nama + " tidak ada di bioskop " + bioskop.getNama());
		}
	}
	public void cancelTicket(Theater bioskop){
		if(punyaTiket.isEmpty()){
			System.out.println("Tiket tidak bisa dikembalikan karena film " + sudahTonton.getFilm().getJudul() + " sudah ditonton oleh " + nama);
		} else{
			Ticket tiketNya = punyaTiket.get(punyaTiket.size()-1);
			Movie filmNya = tiketNya.getFilm();
			String judulNya = tiketNya.getFilm().getJudul();
			int hargaNya = tiketNya.getHarga();
			
			boolean adaFilm = false;
			for(Movie films : bioskop.getFilm()){
				if(films.equals(filmNya)){
					if(bioskop.getSaldo() >= hargaNya){
						System.out.println("Tiket film " + judulNya + " dengan waktu tayang " + tiketNya.getJadwal() + 
										" jenis " + tiketNya.getJenis() + " dikembalikan ke bioskop " + bioskop.getNama());
						bioskop.setSaldo(bioskop.getSaldo() - hargaNya);
						bioskop.pendapatanTotal -= hargaNya;
						punyaTiket.remove(tiketNya);
					} else{
						System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop " + bioskop.getNama() + " lagi tekor...");
					} adaFilm = true;
				}
			}
			if(adaFilm == false){
					System.out.println("Maaf tiket tidak bisa dikembalikan, " + judulNya + " tidak tersedia dalam " + bioskop.getNama());
			}
		}
	}
	public void watchMovie(Ticket tiket){
		if(punyaTiket.contains(tiket)){
			punyaTiket.remove(tiket);
			sudahTonton = tiket;
			System.out.println(nama + " telah menonton film " + tiket.getFilm().getJudul());
		} else{
			System.out.println(nama + " tidak memiliki tiket untuk menonton film " + tiket.getFilm().getJudul());
		}
			
	}
	public boolean contains(Movie[] bioskop, String film) {
        boolean result = false;
        for(Movie films : bioskop){
            if(films.getJudul().equals(film)){
                result = true;
                break;
            }
        }
        return result;
    }
}