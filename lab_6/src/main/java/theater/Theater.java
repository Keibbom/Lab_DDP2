package theater;

import ticket.Ticket;
import movie.Movie;
import java.util.ArrayList;

public class Theater{
	public static int pendapatanTotal;
	
	//Atribut
	private String nama;
	private int saldo;
	private ArrayList<Ticket> tiket;
	private Movie[] film;
	
	//Konstruktor
	public Theater(String nama, int saldo, ArrayList<Ticket> tiket, Movie[] film){
		this.nama = nama;
		this.saldo = saldo;
		this.tiket = tiket;
		this.film = film;
		pendapatanTotal += saldo;
	}
	
	//Mutator & Accessor
	public void setNama(String nama){
		this.nama = nama;
	} public String getNama(){
		return this.nama;
	}
	public void setSaldo(int saldo){
		this.saldo = saldo;
	} public int getSaldo(){
		return this.saldo;
	}
	public void setTiket(ArrayList<Ticket> tiket){
		this.tiket = tiket;
	} public ArrayList<Ticket> getTiket(){
		return this.tiket;
	}
	public void setFilm(Movie[] film){
		this.film = film;
	} public Movie[] getFilm(){
		return this.film;
	}
	
	//Method
	public static void printTotalRevenueEarned(Theater[] theaters){
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + pendapatanTotal);
		System.out.println("------------------------------------------------------------------");
		for(Theater bioskop : theaters){
			System.out.println("Bioskop\t\t: " + bioskop.getNama() + "\nSaldo Kas\t: " + bioskop.getSaldo() + "\n");
		}
		System.out.println("------------------------------------------------------------------");
	}
	public Ticket beliTiket(String judul, String jadwal, boolean tigaDimensi){
		Ticket tiketBaru = null;
		for(Ticket tiketNya : this.tiket){
			if(tiketNya.getFilm().getJudul().equals(judul) && tiketNya.getJadwal().equals(jadwal) && tiketNya.getTigaDimensi() == tigaDimensi){
				tiketBaru = tiketNya;
			} 
		}
		return tiketBaru;
	}
		
	//Informasi
	public void printInfo(){
		String daftarFilm = "";
		for(Movie i : film){
			if(i.equals(film[film.length-1])){
				daftarFilm += i.getJudul();
			} else{
				daftarFilm += i.getJudul() + ", ";
			}
		}
		System.out.println("------------------------------------------------------------------\n" +
			"Bioskop\t\t\t: " + nama + "\n" +
			"Saldo Kas\t\t: " + saldo + "\n" +
			"Jumlah tiket tersedia\t: " + tiket.size() + "\n" +
			"Daftar Film tersedia\t: " + daftarFilm + "\n" +
			"------------------------------------------------------------------");
	}
}